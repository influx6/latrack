package api_test

import (
	"encoding/json"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/influx6/latrack/internal/api"
)

func TestGeoArea_UnmarshalJSON(t *testing.T) {
	testCases := []struct {
		JSON          string
		Expected      api.GeoArea
		ErrorExpected bool
	}{
		{
			JSON: `{
				"origin": ["50.85132920000000212", "5.70318029999999965"],
				"destination": ["53.2315726000000069", "6.52567179999999958"]
			}`,
			Expected: api.GeoArea{
				Origin: api.Location{
					Latitude:  50.85132920000000212,
					Longitude: 5.70318029999999965,
				},
				Destination: api.Location{
					Latitude:  53.2315726000000069,
					Longitude: 6.52567179999999958,
				},
			},
		},
		{
			JSON: `{
				"origin": ["150.85132920000000212", "5.70318029999999965"],
				"destination": ["53.2315726000000069", "296.52567179999999958"]
			}`,
			ErrorExpected: true,
		},
	}

	for _, testCase := range testCases {
		var area api.GeoArea
		err := json.Unmarshal([]byte(testCase.JSON), &area)
		if testCase.ErrorExpected {
			assert.Error(t, err)
			continue
		}

		assert.NoError(t, err)
		assert.True(t, reflect.DeepEqual(testCase.Expected, area))
	}
}
