package api

import (
	"errors"
	"regexp"

	"strconv"

	"encoding/json"
)

//********************************************************
// OrderStorage
//********************************************************

// OrderStatus defines a string type which is used to represent
// different order states.
type OrderStatus string

// constants of order types
const (
	UNASSIGNED OrderStatus = "UNASSIGN"
	TAKEN      OrderStatus = "TAKEN"
)

// Order represents a standing delivery order.
type Order struct {
	ID       string      `json:"id"`
	Distance float64     `json:"distance"`
	Status   OrderStatus `json:"status"`
}

//********************************************************
// DistanceService
//********************************************************

var (
	latitudeRegexString  = regexp.MustCompile("^[-+]?([1-8]?\\d(\\.\\d+)?|90(\\.0+)?)$")
	longitudeRegexString = regexp.MustCompile("^[-+]?(180(\\.0+)?|((1[0-7]\\d)|([1-9]?\\d))(\\.\\d+)?)$")
)

// DistanceService defines a implementation which will provide
// a distance between the origin and destination of a GeoArea.
// It will implement has desired the necessary logic to return
// a suitable, and accurate distance to be crossed to reach
// destination from origin.
type DistanceService interface {
	GetDistance(GeoArea) (float64, error)
}

// Location represent a giving latitude and longitude location.
type Location struct {
	Latitude  float64
	Longitude float64
}

// GeoArea represents a giving area of geographic spanning
// an area represented by it's origin and destination's locations,
// each with their respective latitude and longitude coordinates.
type GeoArea struct {
	Origin      Location
	Destination Location
}

// UnmarshalJSON implements the json.Unmarshaler interface.
func (g *GeoArea) UnmarshalJSON(b []byte) error {
	incoming := struct {
		Origin      []string `json:"origin"`
		Destination []string `json:"destination"`
	}{}

	var err error
	if err = json.Unmarshal(b, &incoming); err != nil {
		return err
	}

	if len(incoming.Destination) != 2 {
		return errors.New("invalid length of destination list")
	}

	if len(incoming.Origin) != 2 {
		return errors.New("invalid length of origin list")
	}

	if !latitudeRegexString.MatchString(incoming.Origin[0]) {
		return errors.New("origin latitude value is invalid")
	}

	if !longitudeRegexString.MatchString(incoming.Origin[1]) {
		return errors.New("origin longitude value is invalid")
	}

	if !latitudeRegexString.MatchString(incoming.Destination[0]) {
		return errors.New("destination latitude value is invalid")
	}

	if !longitudeRegexString.MatchString(incoming.Destination[1]) {
		return errors.New("destination longitude value is invalid")
	}

	if g.Origin.Latitude, err = strconv.ParseFloat(incoming.Origin[0], 64); err != nil {
		return err
	}

	if g.Origin.Longitude, err = strconv.ParseFloat(incoming.Origin[1], 64); err != nil {
		return err
	}

	if g.Destination.Latitude, err = strconv.ParseFloat(incoming.Destination[0], 64); err != nil {
		return err
	}

	if g.Destination.Longitude, err = strconv.ParseFloat(incoming.Destination[1], 64); err != nil {
		return err
	}

	return nil
}
