package api

import (
	"errors"
	"sync"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

//*******************************************
// Map implementation of OrderStorage
//*******************************************

var _ Orders = &MapOrderStore{}

// MapOrderStore implements the OrderStorage uses a concurrently safe
// hashmap.
type MapOrderStore struct {
	ml     sync.RWMutex
	orders map[string]Order
}

// NewMapOrderStore returns a new instance of MapOrderStore.
func NewMapOrderStore() *MapOrderStore {
	return &MapOrderStore{
		orders: map[string]Order{},
	}
}

// Add adds giving order into the database collection.
func (mos *MapOrderStore) Add(order Order) error {
	mos.ml.Lock()
	defer mos.ml.Unlock()
	if _, ok := mos.orders[order.ID]; ok {
		return errors.New("id already exists")
	}
	mos.orders[order.ID] = order
	return nil
}

// Count returns total orders available.
func (mos *MapOrderStore) Count() (int, error) {
	mos.ml.RLock()
	defer mos.ml.RUnlock()
	return len(mos.orders), nil
}

// Update attempts to update an existing order record into
// the database collection.
func (mos *MapOrderStore) Update(order Order) error {
	mos.ml.Lock()
	defer mos.ml.Unlock()
	if _, ok := mos.orders[order.ID]; ok {
		mos.orders[order.ID] = order
		return nil
	}
	return errors.New("not found")
}

// Get returns an existing Order from underline collection in database.
// It returns an error if not found or if an error occurred.
func (mos *MapOrderStore) Get(id string) (Order, error) {
	mos.ml.RLock()
	defer mos.ml.RUnlock()
	if order, ok := mos.orders[id]; ok {
		return order, nil
	}
	return Order{}, errors.New("not found")
}

// All returns all storage orders if no error occured throug the
// data based connection.
func (mos *MapOrderStore) All(page int, limit int) ([]Order, error) {
	mos.ml.RLock()
	defer mos.ml.RUnlock()

	orders := make([]Order, 0, len(mos.orders))
	for _, order := range mos.orders {
		orders = append(orders, order)
	}

	if page >= 0 && limit > 0 {
		lastPage := 1
		if page > 1 {
			lastPage = page - 1
		}

		if page > 0 {
			skip := lastPage * limit
			if len(orders) <= skip {
				return orders[:0], nil
			}

			left := orders[skip:]
			if limit >= len(left) {
				return left, nil
			}

			return left[:limit], nil
		}

		if limit < len(orders) {
			return orders[:limit], nil
		}

		return orders, nil
	}

	if page <= 0 && limit > 0 {
		if limit < len(orders) {
			return orders[:limit], nil
		}
	}

	return orders, nil
}

//*******************************************
// MongoDB implementation of OrderStorage
//*******************************************

var _ Orders = &MgoOrderStore{}

// MgoOrderStore implements a OrderStorage using a mongodb as
// the underline storage.
type MgoOrderStore struct {
	db         string
	collection string
	master     *mgo.Session
}

// NewMgoOrderStore returns a new instance of a MgoOrderStore.
func NewMgoOrderStore(database string, col string, master *mgo.Session) *MgoOrderStore {
	return &MgoOrderStore{
		db:         database,
		collection: col,
		master:     master,
	}
}

// Add adds giving order into the database collection.
func (mos *MgoOrderStore) Add(order Order) error {
	// We will be handling a write, to ensure we don't get blocked
	// or are blocking reads, we will copy instead, which uses another
	// socket connection either created or available within the pool.
	// mgo and mongodb handles the consistency guarantees majorly when
	// using monotonic guarantees for consistent read after writes.
	session := mos.master.Copy()
	defer session.Close()

	db := session.DB(mos.db)
	col := db.C(mos.collection)
	return col.Insert(order)
}

// Update attempts to update an existing order record into
// the database collection.
func (mos *MgoOrderStore) Update(order Order) error {
	// We will be handling a write, to ensure we don't get blocked
	// or are blocking reads, we will copy instead, which uses another
	// socket connection either created or available within the pool.
	// mgo and mongodb handles the consistency guarantees majorly when
	// using monotonic guarantees for consistent read after writes.
	session := mos.master.Copy()
	defer session.Close()

	db := session.DB(mos.db)
	col := db.C(mos.collection)

	return col.Update(bson.M{
		"id": order.ID,
	}, bson.M{
		"$set": bson.M{
			"status": order.Status,
		},
	})
}

// AddIndex adds giving indexes into collection in database.
func (mos *MgoOrderStore) AddIndex(index mgo.Index) error {
	session := mos.master.Copy()
	defer session.Close()

	db := session.DB(mos.db)
	col := db.C(mos.collection)

	return col.EnsureIndex(index)
}

// Count returns total Order records in database.
func (mos *MgoOrderStore) Count() (int, error) {
	session := mos.master.Clone()
	db := session.DB(mos.db)
	col := db.C(mos.collection)

	return col.Find(nil).Count()
}

// Get returns an existing Order from underline collection in database.
// It returns an error if not found or if an error occurred.
func (mos *MgoOrderStore) Get(id string) (Order, error) {
	session := mos.master.Clone()
	db := session.DB(mos.db)
	col := db.C(mos.collection)

	var order Order
	if err := col.Find(bson.M{"id": id}).One(&order); err != nil {
		return order, err
	}
	return order, nil
}

// All returns all storage orders if no error occurred through the
// data based connection.
func (mos *MgoOrderStore) All(page int, limit int) ([]Order, error) {
	// We will be retrieving data from the database, which is
	// basically a read, we can optimize the connection pool
	// by simply reusing an existing connection, as mgo.Session.Copy
	// creates a new connection into the pool.
	session := mos.master.Clone()
	db := session.DB(mos.db)
	col := db.C(mos.collection)

	var orders []Order
	if page <= 0 && limit > 0 {
		if err := col.Find(nil).Limit(limit).All(&orders); err != nil {
			return nil, err
		}
		return orders, nil
	}

	if page >= 0 && limit > 0 {
		lastPage := 1
		if page > 1 {
			lastPage = page - 1
		}

		if page > 0 {
			if err := col.Find(nil).Skip(limit * lastPage).Limit(limit).All(&orders); err != nil {
				return nil, err
			}
			return orders, nil
		}

		if err := col.Find(nil).Limit(limit).All(&orders); err != nil {
			return nil, err
		}

		return orders, nil
	}

	if page < 0 && limit > 0 {
		if err := col.Find(nil).Limit(limit).All(&orders); err != nil {
			return nil, err
		}
	}

	if err := col.Find(nil).All(&orders); err != nil {
		return nil, err
	}
	return orders, nil
}
