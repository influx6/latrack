package api_test

import (
	"fmt"
	"net/http"
	"testing"

	"bytes"
	"net/http/httptest"

	"encoding/json"
	"reflect"

	"github.com/gorilla/mux"
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/influx6/latrack/internal/api"
)

func createHandler() http.Handler {
	var distances api.ApproxLocations
	db := api.NewMapOrderStore()

	app := mux.NewRouter()
	app.HandleFunc("/orders", api.AllOrdersHandler(db)).Methods("GET")
	app.HandleFunc("/order/{id}", api.TakeOrderHandler(db)).Methods("PUT")
	app.HandleFunc("/order", api.NewOrderHandler(db, distances)).Methods("POST")
	return app
}

func TestNewOrderHandler(t *testing.T) {
	t.Log("Should be create new orders")
	{
		handler := createHandler()
		testCases := []struct {
			ExpectedStatus int
			ReqBody        []byte
			ResBody        []byte
		}{
			{
				ExpectedStatus: 200,
				ReqBody: []byte(`{
					"origin": ["51.925146", "5.70318029999999965"],
					"destination": ["53.2315726000000069", "6.52567179999999958"]
				}`),
				ResBody: []byte("{\"id\":\"eb0d0ea9-6eca-4d51-b51d-916fe802244c\",\"distance\":155.5334332389984,\"status\":\"UNASSIGN\"}\n"),
			},
		}

		for _, testCase := range testCases {
			req, err := http.NewRequest("POST", "/order", bytes.NewBuffer(testCase.ReqBody))
			assert.NoError(t, err)
			assert.NotNil(t, req)

			res := httptest.NewRecorder()
			handler.ServeHTTP(res, req)

			assert.Equal(t, res.Code, testCase.ExpectedStatus)
			assert.NotNil(t, res.Body)

			if testCase.ResBody == nil {
				continue
			}

			receivedBody := map[string]interface{}{}
			assert.NoError(t, json.NewDecoder(res.Body).Decode(&receivedBody))

			expectedBody := map[string]interface{}{}
			assert.NoError(t, json.NewDecoder(bytes.NewBuffer(testCase.ResBody)).Decode(&expectedBody))

			// if id to ensure similarity of value.
			expectedBody["id"] = receivedBody["id"]

			assert.True(t, reflect.DeepEqual(expectedBody, receivedBody))
		}
	}
	t.Log("Should fail to create new order with invalid location values")
	{
		handler := createHandler()
		testCases := []struct {
			ExpectedStatus int
			ReqBody        []byte
			ResBody        []byte
		}{
			{
				ExpectedStatus: 500,
				ReqBody: []byte(`{
					"origin": ["50.85132920000000212", "500.70318029999999965"],
					"destination": ["50.2315726000000069", "45.52567179999999958"]
				}`),
			},
			{
				ExpectedStatus: 500,
				ReqBody: []byte(`{
					"origin": ["550.85132920000000212", "5.70318029999999965"],
					"destination": ["503.2315726000000069", "156.52567179999999958"]
				}`),
			},
			{
				ExpectedStatus: 500,
				ReqBody: []byte(`{
					"origin": ["150.85132920000000212", "5.70318029999999965"],
					"destination": ["53.2315726000000069", "296.52567179999999958"]
				}`),
			},
		}

		for _, testCase := range testCases {
			req, err := http.NewRequest("POST", "/order", bytes.NewBuffer(testCase.ReqBody))
			assert.NoError(t, err)
			assert.NotNil(t, req)

			res := httptest.NewRecorder()
			handler.ServeHTTP(res, req)

			assert.Equal(t, res.Code, testCase.ExpectedStatus)
			if testCase.ResBody == nil {
				continue
			}

			assert.NotNil(t, res.Body)

			var received api.Order
			assert.NoError(t, json.NewDecoder(res.Body).Decode(&received))

			var expected api.Order
			assert.NoError(t, json.NewDecoder(bytes.NewBuffer(testCase.ResBody)).Decode(&expected))
			assert.True(t, reflect.DeepEqual(expected, received))
		}
	}
}

func TestTakeOrderHandler(t *testing.T) {
	handler := createHandler()
	body := bytes.NewBuffer([]byte(`{
		"origin": ["51.925146", "5.70318029999999965"],
		"destination": ["53.2315726000000069", "6.52567179999999958"]
	}`))
	req, err := http.NewRequest("POST", "/order", body)
	assert.NoError(t, err)
	assert.NotNil(t, req)

	res := httptest.NewRecorder()
	handler.ServeHTTP(res, req)

	assert.NotNil(t, res.Body)
	assert.Equal(t, http.StatusOK, res.Code)

	var order api.Order
	assert.NoError(t, json.NewDecoder(res.Body).Decode(&order))

	req2, err := http.NewRequest("PUT", "/order/"+order.ID, bytes.NewBuffer([]byte(`{"status": "taken"}`)))
	assert.NoError(t, err)
	assert.NotNil(t, req2)

	res2 := httptest.NewRecorder()
	handler.ServeHTTP(res2, req2)
	assert.Equal(t, http.StatusOK, res2.Code)
	assert.NotNil(t, res2.Body)

	received := map[string]interface{}{}
	assert.NoError(t, json.NewDecoder(res2.Body).Decode(&received))
	assert.Equal(t, received["status"], "SUCCESS")

	req3, err := http.NewRequest("PUT", "/order/"+order.ID, bytes.NewBuffer([]byte(`{"status": "taken"}`)))
	assert.NoError(t, err)
	assert.NotNil(t, req3)

	res3 := httptest.NewRecorder()
	handler.ServeHTTP(res3, req3)
	assert.Equal(t, http.StatusConflict, res3.Code)
	assert.NotNil(t, res3.Body)

	errorRec := map[string]interface{}{}
	assert.NoError(t, json.NewDecoder(res3.Body).Decode(&errorRec))
	assert.Equal(t, errorRec["error"], "ORDER_ALREADY_BEEN_TAKEN")
}

func TestAllOrdersHandler(t *testing.T) {
	t.Log("Should be retrieve all orders")
	{
		handler := createHandler()
		body := bytes.NewBuffer([]byte(`{
		"origin": ["51.925146", "5.70318029999999965"],
		"destination": ["53.2315726000000069", "6.52567179999999958"]
	}`))
		req, err := http.NewRequest("POST", "/order", body)
		assert.NoError(t, err)
		assert.NotNil(t, req)

		res := httptest.NewRecorder()
		handler.ServeHTTP(res, req)
		assert.NotNil(t, res.Body)
		assert.Equal(t, http.StatusOK, res.Code)

		req2, err := http.NewRequest("GET", "/orders", nil)
		assert.NoError(t, err)
		assert.NotNil(t, req2)

		res2 := httptest.NewRecorder()
		handler.ServeHTTP(res2, req2)
		assert.NotNil(t, res2.Body)
		assert.Equal(t, http.StatusOK, res2.Code)

		var orders []api.Order
		assert.NoError(t, json.NewDecoder(res2.Body).Decode(&orders))
		assert.Len(t, orders, 1)
	}

	t.Log("Should be retrieve orders with page and limit query parameters")
	{
		db := api.NewMapOrderStore()
		for i := 0; i < 100; i++ {
			var order api.Order
			order.Distance = 314.545
			order.Status = api.UNASSIGNED
			order.ID = uuid.NewV4().String()
			assert.NoError(t, db.Add(order))
		}

		var distances api.ApproxLocations
		app := mux.NewRouter()
		app.HandleFunc("/orders", api.AllOrdersHandler(db)).Methods("GET")
		app.HandleFunc("/order/{id}", api.TakeOrderHandler(db)).Methods("PUT")
		app.HandleFunc("/order", api.NewOrderHandler(db, distances)).Methods("POST")

		testCases := []struct {
			Page     int
			Limit    int
			Expected int
		}{
			{
				Page:     0,
				Limit:    -1,
				Expected: 100,
			},
			{
				Page:     0,
				Limit:    2,
				Expected: 2,
			},
			{
				Page:     1,
				Limit:    10,
				Expected: 10,
			},
			{
				Page:     2,
				Limit:    20,
				Expected: 20,
			},
			{
				Page:     1,
				Limit:    100,
				Expected: 0,
			},
			{
				Page:     1,
				Limit:    50,
				Expected: 50,
			},
			{
				Page:     2,
				Limit:    50,
				Expected: 50,
			},
		}

		for _, testCase := range testCases {
			path := fmt.Sprintf("/orders?page=%d&limit=%d", testCase.Page, testCase.Limit)
			req, err := http.NewRequest("GET", path, nil)
			assert.NoError(t, err)
			assert.NotNil(t, req)

			res := httptest.NewRecorder()
			app.ServeHTTP(res, req)

			assert.Equal(t, http.StatusOK, res.Code)
			assert.NotNil(t, res.Body)

			var orders []api.Order
			assert.NoError(t, json.NewDecoder(res.Body).Decode(&orders))

			assert.Len(t, orders, testCase.Expected)
		}
	}
}
