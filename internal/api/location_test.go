package api_test

import (
	"testing"
	"time"

	"net/http"

	"github.com/stretchr/testify/assert"
	"gitlab.com/influx6/latrack/internal/api"
)

var (
	destGeoLatitude  = 51.925146
	destGeoLongitude = 4.478617
	originLatitude   = 37.1768672
	originLongitude  = -3.608897
)

func TestGoogleMapLocations_GetDistance(t *testing.T) {
	t.Logf("Should be able to get distance using GoogleMaps Route API")
	{
		var area api.GeoArea
		area.Origin.Latitude = originLatitude
		area.Origin.Longitude = originLongitude
		area.Destination.Latitude = destGeoLatitude
		area.Destination.Longitude = destGeoLongitude

		var gapi api.GoogleMapLocations
		gapi.Client = &http.Client{Timeout: time.Second * 5}

		distance, err := gapi.GetDistance(area)
		assert.NoError(t, err)
		assert.Equal(t, distance, 2121.608)
	}
}

func TestApproxLocations_GetDistance(t *testing.T) {
	t.Logf("Should be able to get distance using local Approximation API")
	{
		var area api.GeoArea
		area.Origin.Latitude = originLatitude
		area.Origin.Longitude = originLongitude
		area.Destination.Latitude = destGeoLatitude
		area.Destination.Longitude = destGeoLongitude

		var api api.ApproxLocations
		distance, err := api.GetDistance(area)
		assert.NoError(t, err)
		assert.Equal(t, distance, 1758.0806131200138)
	}
}
