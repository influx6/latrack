package api_test

import (
	"flag"
	"reflect"
	"testing"
	"time"

	"github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/influx6/latrack/internal/api"
	"gopkg.in/mgo.v2"
)

func testAllOrders_All(t *testing.T, adder api.AddOrder, orders api.AllOrders) {
	for i := 0; i < 100; i++ {
		var order api.Order
		order.Distance = 314.545
		order.Status = api.UNASSIGNED
		order.ID = uuid.NewV4().String()
		assert.NoError(t, adder.Add(order))
	}

	tests := []struct {
		Page          int
		Limit         int
		Expected      int
		ExpectedError bool
	}{
		{
			Page:     0,
			Limit:    -1,
			Expected: 100,
		},
		{
			Page:     0,
			Limit:    2,
			Expected: 2,
		},
		{
			Page:     1,
			Limit:    10,
			Expected: 10,
		},
		{
			Page:     2,
			Limit:    20,
			Expected: 20,
		},
		{
			Page:     1,
			Limit:    100,
			Expected: 0,
		},
		{
			Page:     1,
			Limit:    50,
			Expected: 50,
		},
		{
			Page:     2,
			Limit:    50,
			Expected: 50,
		},
	}

	for _, testCase := range tests {
		list, err := orders.All(testCase.Page, testCase.Limit)
		if testCase.ExpectedError {
			assert.Error(t, err)
		} else {
			assert.NoError(t, err)
		}

		assert.Len(t, list, testCase.Expected)
	}
}

func TestNewMapOrderStore(t *testing.T) {
	t.Logf("Should be able to handle CRUD operations for orders")
	{
		db := api.NewMapOrderStore()

		var order api.Order
		order.Distance = 314.545
		order.Status = api.UNASSIGNED
		order.ID = uuid.NewV4().String()

		assert.NoError(t, db.Add(order))

		_, getErr := db.Get(uuid.NewV4().String())
		assert.Error(t, getErr)

		retrieved, err := db.Get(order.ID)
		assert.True(t, reflect.DeepEqual(order, retrieved))

		order.Status = api.TAKEN
		assert.Equal(t, api.TAKEN, order.Status)
		assert.NoError(t, db.Update(order))

		retrieved2, err := db.Get(order.ID)
		assert.True(t, reflect.DeepEqual(order, retrieved2))

		orders, err := db.All(-1, -1)
		assert.NoError(t, err)
		assert.Len(t, orders, 1)
	}

	t.Logf("Should be able to retrieve orders based on page and limit scope")
	{
		db := api.NewMapOrderStore()
		testAllOrders_All(t, db, db)
	}
}

var (
	addr     = flag.String("mgo_addr", "0.0.0.0:27017", "address for mongodb instance")
	authdb   = flag.String("mgo_authdb", "", "data for mongodb to be used")
	username = flag.String("mgo_username", "", "username for mongodb if any")
	password = flag.String("mgo_password", "", "password for mongodb authentication if any")
)

func TestNewMgoOrderStore(t *testing.T) {
	if *addr == "" || *authdb == "" {
		t.Skip("Requires -mgo_addr and -mgo_authdb arguments atleast to run. See code for other arguments")
		return
	}

	master, err := mgo.DialWithInfo(&mgo.DialInfo{
		Addrs:    []string{*addr},
		Username: *username,
		Password: *password,
		Database: *authdb,
		Timeout: 20 * time.Second,
	})

	assert.NoError(t, err)
	assert.NotNil(t, master)

	master.SetMode(mgo.Monotonic, true)

	t.Logf("Should be able to handle CRUD operations for orders")
	{
		defer master.DB(*authdb).C("orders_test").DropCollection()

		db := api.NewMgoOrderStore(*authdb, "orders_test", master)

		var order api.Order
		order.Distance = 314.545
		order.Status = api.UNASSIGNED
		order.ID = uuid.NewV4().String()

		assert.NoError(t, db.Add(order))

		_, getErr := db.Get(uuid.NewV4().String())
		assert.Error(t, getErr)

		retrieved, err := db.Get(order.ID)
		assert.True(t, reflect.DeepEqual(order, retrieved))

		order.Status = api.TAKEN
		assert.Equal(t, api.TAKEN, order.Status)
		assert.NoError(t, db.Update(order))

		retrieved2, err := db.Get(order.ID)
		assert.True(t, reflect.DeepEqual(order, retrieved2))

		orders, err := db.All(-1, -1)
		assert.NoError(t, err)
		assert.Len(t, orders, 1)

	}

	t.Logf("Should be able to retrieve orders based on page and limit scope")
	{

		defer master.DB(*authdb).C("orders_test2").DropCollection()

		db := api.NewMgoOrderStore(*authdb, "orders_test2", master)
		testAllOrders_All(t, db, db)
	}

}
