package api

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/satori/go.uuid"
)

const (
	successMessage = "SUCCESS"
	takenMessage   = "ORDER_ALREADY_BEEN_TAKEN"
)

// Orders defines an interface which composes the AddOrder, AllOrder and GetUpdateOrders
// interface as a single implementation contract.
type Orders interface {
	AddOrder
	AllOrders
	GetUpdateOrders
}

//********************************************************
// HTTP Handlers: Built for gorilla mux
//********************************************************

// GetUpdateOrder defines an interface which exposes methods to retrieve and update a giving Order.
type GetUpdateOrders interface {
	Get(id string) (Order, error)
	Update(order Order) error
}

// TakeOrderHandler returns a http.HandlerFunc which services the allocation of
// a pending order to be shifted into a taken state.
func TakeOrderHandler(storage GetUpdateOrders) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		defer r.Body.Close()

		op := struct {
			Status string `json:"status"`
		}{}
		if err := json.NewDecoder(r.Body).Decode(&op); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			sendError(w, err)
			return
		}

		// TODO(alex): Do we really need to check the content of
		// body, naturally the endpoint represents the action that
		// it should be assigned/taken if unassigned. Therefore is this
		// redundant or do we plan to later consider status values that
		// can change the state of an order later ?
		if op.Status != "taken" {
			w.WriteHeader(http.StatusBadRequest)
			sendError(w, errors.New("invalid status value in body json"))
			return
		}

		params := mux.Vars(r)
		id, ok := params["id"]
		if !ok {
			w.WriteHeader(http.StatusBadRequest)
			sendError(w, errors.New("id not provided"))
			return
		}

		order, err := storage.Get(id)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			sendError(w, err)
			return
		}

		if order.Status == TAKEN {
			w.WriteHeader(http.StatusConflict)
			sendMessageError(w, takenMessage)
			return
		}

		order.Status = TAKEN
		if err := storage.Update(order); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			sendError(w, err)
			return
		}

		w.WriteHeader(200)
		if err := json.NewEncoder(w).Encode(map[string]interface{}{"status": successMessage}); err != nil {
			log.Printf("Failed to write response for request: %+q", err)
			w.WriteHeader(http.StatusInternalServerError)
			sendError(w, err)
		}
	}
}

// AllOrders defines an interface which exposes a method for retrieving all orders.
type AllOrders interface {
	All(page int, limit int) ([]Order, error)
}

// AllOrdersHandler returns a http.HandlerFunc which services
// request to retrieve all existing orders.
func AllOrdersHandler(storage AllOrders) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := r.ParseForm(); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			sendError(w, err)
			return
		}

		page, err := strconv.Atoi(r.Form.Get("page"))
		if err != nil {
			page = -1
		}

		limit, err := strconv.Atoi(r.Form.Get("limit"))
		if err != nil {
			limit = -1
		}

		orders, err := storage.All(page, limit)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			sendError(w, err)
			return
		}

		w.WriteHeader(200)
		if err := json.NewEncoder(w).Encode(orders); err != nil {
			log.Printf("Failed to write response for request: %+q", err)
			w.WriteHeader(http.StatusInternalServerError)
			sendError(w, err)
		}
	}
}

// AddOrder defines an interface which exposes a method to add giving Order.
type AddOrder interface {
	Add(order Order) error
}

// NewOrderHandler returns a http. handler which handles addition of incoming
// order json into underline storage. We assume json is delivered
// else an error response will be returned for request.
func NewOrderHandler(storage AddOrder, distances DistanceService) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		defer r.Body.Close()

		// Retrieve expected json containing target location
		// for delivery.
		var area GeoArea
		if err := json.NewDecoder(r.Body).Decode(&area); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			sendError(w, err)
			return
		}

		distance, err := distances.GetDistance(area)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			sendError(w, err)
			return
		}

		var order Order
		order.ID = uuid.NewV4().String()
		order.Status = UNASSIGNED
		order.Distance = distance

		if err := storage.Add(order); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			sendError(w, err)
			return
		}

		w.WriteHeader(200)
		if err := json.NewEncoder(w).Encode(order); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			sendError(w, err)
			return
		}
	}
}

func sendError(w http.ResponseWriter, err error) {
	if werr := json.NewEncoder(w).Encode(struct {
		Error string `json:"error"`
	}{Error: err.Error()}); werr != nil {
		log.Printf("Error writing message into response: %+q", werr)
	}
}

func sendMessageError(w http.ResponseWriter, msg string) {
	if werr := json.NewEncoder(w).Encode(struct {
		Error string `json:"error"`
	}{Error: msg}); werr != nil {
		log.Printf("Error writing message into response: %+q", werr)
	}
}
