package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"math"
	"net/http"
)

//********************************************************
// GoogleMapLocations: Google Maps based DistanceService
//********************************************************

const (
	distanceURL = "http://maps.googleapis.com/maps/api/distancematrix"
)

// GoogleMapLocations implements the DistanceService by using the publicly
// available GoogleMaps API to calculate needed distances for giving
// GeoArea.
type GoogleMapLocations struct {
	Client *http.Client
}

// GetDistance implements the DistanceService interface.
// We return distance values in kilometers.
func (g GoogleMapLocations) GetDistance(area GeoArea) (float64, error) {
	path := fmt.Sprintf("%s/json?units=metrics&origins=%.7f,%.7f&destinations=%.7f,%.7f", distanceURL,
		area.Origin.Latitude, area.Origin.Longitude,
		area.Destination.Latitude, area.Destination.Longitude,
	)

	req, err := http.NewRequest("GET", path, nil)
	if err != nil {
		return -1, err
	}

	res, err := g.Client.Do(req)
	if err != nil {
		return -1, err
	}

	defer res.Body.Close()

	var response directionResponse
	if err := json.NewDecoder(res.Body).Decode(&response); err != nil {
		return -1, err
	}

	if response.Status != "OK" {
		return -1, errors.New(response.Status)
	}

	if len(response.Data) == 0 {
		return -1, errors.New("no data received")
	}

	target := response.Data[0]
	if len(target.Elements) == 0 {
		return -1, errors.New("no data received")
	}

	return target.Elements[0].Distance.Value / 1000, nil
}

type directionResponse struct {
	Status       string   `json:"status"`
	Origins      []string `json:"origin_addresses"`
	Destinations []string `json:"destination_addresses"`
	Data         []struct {
		Elements []distanceData `json:"elements"`
	} `json:"rows"`
}

type distanceData struct {
	Status   string    `json:"status"`
	Duration valueData `json:"duration"`
	Distance valueData `json:"distance"`
}

type valueData struct {
	Value float64 `json:"value"`
	Text  string  `json:"text"`
}

//********************************************************
// ApproxLocations: Approximation based DistanceService
//********************************************************

const (
	earthRadius = 6371 // in kilometers
)

// ApproxLocations implements the DistanceService providing
// a approximation of distance between two geo-location points
// using the Haversine method.
type ApproxLocations struct{}

// GetDistance implements the DistanceService interface.
// Assumes that GeoArea latitude and longitude values are in degrees.
func (ApproxLocations) GetDistance(area GeoArea) (float64, error) {
	return greatCircleDistance(
		toRadians(area.Origin.Latitude), toRadians(area.Origin.Longitude),
		toRadians(area.Destination.Latitude), toRadians(area.Destination.Longitude),
	), nil
}

// greatCircleDistance calculates the great-circle distance over a spherical domain (e.g earth)
// for the distance between two points on the sphere. It uses the haversine method.
// We assume the values are in radians and we return values in kilometers.
func greatCircleDistance(lat1, long1, lat2, long2 float64) float64 {
	latDiff := lat2 - lat1
	longDiff := long2 - long1
	latDiffMid := latDiff / 2
	longDiffMid := longDiff / 2

	latMidSin := math.Sin(latDiffMid)
	longMidSin := math.Sin(longDiffMid)

	a := (latMidSin * latMidSin) +
		(math.Cos(lat1)*math.Cos(lat2))*(longMidSin*longMidSin)

	c := 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))

	return earthRadius * c
}

func toRadians(t float64) float64 {
	return (t * math.Pi) / 180
}
