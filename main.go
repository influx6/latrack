package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
	"strings"
	"net"

	"github.com/gorilla/mux"
	"gitlab.com/influx6/latrack/internal/api"
	"gopkg.in/mgo.v2"
)

var (
	port          = os.Getenv("PORT")
	host          = os.Getenv("HOST")
	mongoAddrs     = os.Getenv("MONGODB_ADDRS")
	mongoUser     = os.Getenv("MONGODB_USERNAME")
	mongoPassword = os.Getenv("MONGODB_PASSWORD")
	mongoDB       = os.Getenv("MONGODB_DATABASE")
)

func main() {
	logger := log.New(os.Stderr, "", 0)
	masterSession, err := mgo.DialWithInfo(&mgo.DialInfo{
		Addrs:    strings.Split(mongoAddrs, ","),
		Username: mongoUser,
		Password: mongoPassword,
		Database: mongoDB,
		Timeout: 30 * time.Second,
	})

	if err != nil {
		logger.Fatalf("Failed to dial database server: %+q", err)
		return
	}

	masterSession.SetMode(mgo.Monotonic, true)

	db := api.NewMgoOrderStore(mongoDB, "orders", masterSession)
	if err := db.AddIndex(mgo.Index{
		Unique: true,
		Name:   "order_id",
		Key:    []string{"id"},
	}); err != nil {
		logger.Fatalf("Failed to add index into database collection: %+q", err)
		return
	}

	distService := api.GoogleMapLocations{
		Client: &http.Client{
			Timeout: 10 * time.Second,
		},
	}

	app := mux.NewRouter()
	app.HandleFunc("/orders", api.AllOrdersHandler(db)).Methods("GET")
	app.HandleFunc("/order/{id}", api.TakeOrderHandler(db)).Methods("PUT")
	app.HandleFunc("/order", api.NewOrderHandler(db, distService)).Methods("POST")

	addr := net.JoinHostPort(host, port)
	server := &http.Server{Addr: addr, Handler: app}

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, syscall.SIGTERM)

	go func() {
		logger.Printf("HTTP listening on http://%s\n", addr)
		if err := server.ListenAndServe(); err != nil {
			logger.Fatalf("Failed to start server: %+q", err)
			return
		}
	}()

	<-sig

	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	server.Shutdown(ctx)

	logger.Println("Server closed gracefully!")
}
