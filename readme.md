Latrack
-------------
Latrack demonstrates a basic Restful API for order tracking with a limited set of routes.


## Run

You can get up and running quickly using any of the follow.

**Please note that docker is required for this setup to work**

```bash
./start.sh
```

Once command has finished executing, you should be able start hitting api at `http://0.0.0.0:8080`.

## API

#### Add Order (POST /order)

  - Request body:
    It's expected that the coordinate values are in degrees.

    ```
    {
        "origin": ["START_LATITUDE", "START_LONGTITUDE"],
        "destination": ["END_LATITUDE", "END_LONGTITUDE"]
    }
    ```

  - Response:
    Be aware that the distance value is in kilometers (km).

    Header: `HTTP 200`
    Body:
      ```
      {
          "id": <order_id>,
          "distance": <total_distance>,
          "status": "UNASSIGN"
      }
      ```
    or 
    
    Header: `HTTP 500`
    Body:
      ```json
      {
          "error": "ERROR_DESCRIPTION"
      }
      ```

#### Take Order (PUT /order/:id)

  - Request body:

    ```
    {
        "status":"taken"
    }
    ```

  - Response:

    Header: `HTTP 200`
    Body:

      ```
      {
          "status": "SUCCESS"
      }
      ```

    or
    
    Header: `HTTP 409`
    Body:
      ```
      {
          "error": "ORDER_ALREADY_BEEN_TAKEN"
      }
      ```

#### Order List (GET /orders?page=:page&limit=:limit)

  - Response:

    ```
    [
        {
            "id": <order_id>,
            "distance": <total_distance>,
            "status": <ORDER_STATUS>
        },
        ...
    ]
    ```


### The curl Test

- Create an order

```bash
▶ curl http://localhost:8080/order  -d "{\"origin\": [\"51.925146\", \"5.70318029999999965\"],\"destination\": [\"53.2315726000000069\", \"6.52567179999999958\"]}"
{"id":"ebe0361b-9f20-4faa-b5fb-87e7d0b29d63","distance":196.615,"status":"UNASSIGN"}
```

- List all orders

```bash
▶ curl http://localhost:8080/orders
[{"id":"ebe0361b-9f20-4faa-b5fb-87e7d0b29d63","distance":196.615,"status":"UNASSIGN"}]
```

- Take an order

```bash
curl -X PUT -d "{\"status\":\"taken\"}" http://localhost:8080/order/ebe0361b-9f20-4faa-b5fb-87e7d0b29d63
{"status":"SUCCESS"}
```