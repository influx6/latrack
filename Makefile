setup: networkup mongodb interval-10s app interval-5s
	@echo "Ready....."

stop: 
	docker stop latrack-app
	docker stop latrack-db
	docker rm latrack-db
	docker rm latrack-app
	docker network rm latrack-net

ci: setup tests stop

tests: 
	go test --cover ./...
	go test -v ./internal/api/... -mgo_authdb="latrackers" -mgo_username="app" -mgo_password="lappa"

mongodb:
	docker run -d --network latrack-net --name latrack-db \
		-p 27017:27017 \
		-e MONGODB_USERNAME="app" \
		-e MONGODB_PASSWORD="lappa" \
		-e MONGODB_DATABASE="latrackers" \
		-v db:/bitnami bitnami/mongodb:latest

app: build
	docker run -d --network latrack-net --name latrack-app \
		-p 8080:8080 \
		-e PORT=8080 \
		-e HOST="0.0.0.0" \
		-e MONGODB_ADDRS="latrack-db:27017" \
		-e MONGODB_USERNAME="app" \
		-e MONGODB_PASSWORD="lappa" \
		-e MONGODB_DATABASE="latrackers" latrack

build:
	docker build -t latrack .

daemon:
	docker-compose up --detach
	interval-10s

networkup:
	docker network create latrack-net --driver bridge

networkdown:
	docker network rm latrack-net

interval-10s:
	sleep 10s

interval-5s:
	sleep 5s
