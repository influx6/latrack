FROM golang:latest AS build
ADD . /go/src/gitlab.com/influx6/latrack
WORKDIR /go/src/gitlab.com/influx6/latrack
RUN GOOS=linux CGO_ENABLED=0 go build -o latrack main.go

FROM alpine
RUN apk add --update ca-certificates
RUN mkdir /app
WORKDIR /app
COPY --from=build /go/src/gitlab.com/influx6/latrack/latrack /usr/local/bin/latrack
RUN chmod +x /usr/local/bin/latrack
ENV PORT 8080
CMD ["/usr/local/bin/latrack"]

